import React, { Component } from "react";
import { connect } from "react-redux";

class KetQua extends Component {
  render() {
    return (
      <div className="display-4 text-center ">
        <p>{this.props.luaChon}</p>

        <p>{this.props.ketQua}</p>
        <p>Số lượt chơi {this.props.soLuotChoi}</p>
        <p>Số lượt thắng: {this.props.soLuotThang}</p>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    ketQua: state.ketQua,
    luaChon: state.luaChon,
    soLuotChoi: state.soLuotChoi,
    soLuotThang: state.soLuotThang,
  };
};
export default connect(mapStateToProps)(KetQua);
