import { createStore } from "redux";
import { rootReducer } from "../Ex_Shoe_Redux/redux/reducer/root";

export const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
