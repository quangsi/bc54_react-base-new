import axios from "axios";
import React, { Component } from "react";

export default class DemoLifeCycle extends Component {
  componentDidMount() {
    console.log("did mount");
    // gọi api
    axios({
      url: "https://tiki.vn/api/v2/products/widget/delivery_info/9856925?platform=web&pdp=v2",
      method: "GET",
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    console.log("render");
    return <div>DemoLifeCycle</div>;
  }
}
// npm i axios
/**
 * mounting: 1 lần ( sinh ra )
 *
 * updating: nhiều lần ( cập nhật )
 *
 * unmount: 1 lần ( biến mất khỏi layout)
 */
